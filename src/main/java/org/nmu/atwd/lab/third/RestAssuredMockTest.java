package org.nmu.atwd.lab.third;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class RestAssuredMockTest {

    private static final String BASE_URL = "https://b94c95d4-f0fb-4847-b174-433bde92f3e3.mock.pstmn.io";

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = BASE_URL;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

    @Test
    public void verifyGetAction() {
        //success scenario
        given().get("/ownerName/success")
                .then().statusCode(HttpStatus.SC_OK)
                .and().body("name", equalTo("Viktor Myronenko"));
        //unsuccess scenario
        given().get("ownerName/unsuccess")
                .then().statusCode(HttpStatus.SC_FORBIDDEN)
                .and().body("exception", equalTo("I won't say my name"));

    }

    @Test
    public void verifyPostAction() {
        //success scenario
        given().queryParam("permission", "yes")
                .post("/createSomething")
                .then().statusCode(HttpStatus.SC_OK)
                .and().body("result", equalTo("'Nothing' was created"));
        //unsuccess scenario
        given().post("/createSomething")
                .then().statusCode(HttpStatus.SC_BAD_REQUEST)
                .and().body("result", equalTo("You don't have permission to create Something"));
    }

    @Test
    public void verifyPutAction() {
        Map<String, Object> body = Map.of(
                "name", "Viktor",
                "surname", "Myronenko"
                );
        given().body(body).put("/updateMe").then().statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    public void verifyDeleteAction() {
        given().header("SessionID", "123456789").delete("/deleteWorld")
                .then().statusCode(HttpStatus.SC_GONE)
                .and().body("world", equalTo("0"));
    }
}
