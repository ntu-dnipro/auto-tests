package org.nmu.atwd.lab.second;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

public class RestAssuredUserTest {

    private static final String BASE_URL = "https://petstore.swagger.io/v2";
    private static final String USER = "/user";
    private static final String USER_LOGIN = USER + "/login";
    private static final String USER_LOGOUT = USER + "/logout";
    private static final String USER_NAME = USER + "/{username}";
    private static final Faker FAKER = Faker.instance();

    private User user;

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = BASE_URL;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
        user = new User(
                FAKER.name().username(),
                FAKER.harryPotter().character(),
                FAKER.gameOfThrones().character(),
                FAKER.internet().emailAddress(),
                FAKER.internet().password(),
                FAKER.phoneNumber().phoneNumber(),
                1
        );
    }

    @Test
    public void verifyLoginAction() {
        Map<String, String> body = Map.of("username", "ViktorMyronenko", "password", "122m-22-4.20");
        given().body(body);
        Response response = when().get(USER_LOGIN);
        response.then().statusCode(HttpStatus.SC_OK);
        RestAssured.requestSpecification.sessionId(
                response.jsonPath().get("message").toString().replaceAll("[^0-9]]", "")
        );
    }

    @Test(dependsOnMethods = "verifyLoginAction")
    public void verifyCreateAction() {
        Map<String, ?> body = Map.of(
                "username", user.userName,
                "firstName", user.firstName,
                "lastName", user.lastName,
                "email", user.email,
                "password", user.password,
                "phone", user.phone,
                "userStatus", user.status
        );

        given()
                .body(body)
                .post(USER)
                .then().statusCode(HttpStatus.SC_OK);
    }

    @Test(dependsOnMethods = "verifyCreateAction")
    public void verifyGetAction() {
        given().pathParam("username", user.userName)
                .get(USER_NAME).
                then().statusCode(HttpStatus.SC_OK)
                .and().body("firstName", equalTo(user.firstName));
    }

    @Test(dependsOnMethods = "verifyGetAction")
    public void verifyDeleteAction() {
        given().pathParam("username", user.userName)
                .delete(USER_NAME)
                .then().statusCode(HttpStatus.SC_OK);
    }

    @Test(dependsOnMethods = "verifyLoginAction")
    public void verifyLogoutAction() {
        given().get(USER_LOGOUT).then().statusCode(HttpStatus.SC_OK);
    }

    record User(
            String userName,
            String firstName,
            String lastName,
            String email,
            String password,
            String phone,
            Integer status) {}
}
