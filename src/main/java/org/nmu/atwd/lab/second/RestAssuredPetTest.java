package org.nmu.atwd.lab.second;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;

public class RestAssuredPetTest {

    private static final String BASE_URL = "https://petstore.swagger.io/v2";
    private static final String USER_LOGIN =  "/user/login";
    private static final String USER_LOGOUT =  "/user/logout";
    private static final String PET = "/pet";
    private static final String PET_ID = PET + "/{petId}";

    private final Pet pet = new Pet();

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = BASE_URL;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
        Map<String, String> body = Map.of("username", "ViktorMyronenko", "password", "122m-22-4.20");
        given().body(body);
        Response response = when().get(USER_LOGIN);
        response.then().statusCode(HttpStatus.SC_OK);
        RestAssured.requestSpecification.sessionId(
                response.jsonPath().get("message").toString().replaceAll("[^0-9]]", "")
        );
        pet.setName("Viktor");
        pet.setPhotoUrls(List.of("photo"));
        pet.setStatus("in-stock");
        Category category = new Category();
        category.setId(4L);
        category.setName("122m-22-4.20");
        pet.setCategory(category);
        Tag tag = new Tag();
        tag.setId(1L);
        tag.setName("cat");
        pet.setTags(List.of(tag));
    }

    @Test
    public void verifyCreateAction() {
        Response response = given().body(pet).post(PET);
        response.then().statusCode(HttpStatus.SC_OK);
        pet.setId(response.jsonPath().get("id"));
    }

    @Test(dependsOnMethods = "verifyCreateAction")
    public void verifyGetAction() {
        given().pathParam("petId",pet.getId())
                .get(PET_ID).then().statusCode(HttpStatus.SC_OK)
                .and().body("name", equalTo(pet.name));
    }

    @Test(dependsOnMethods = "verifyGetAction")
    public void verifyUpdateAction() {
        pet.setStatus("out-of-stock");
        given().body(pet).put(PET).
                then().statusCode(HttpStatus.SC_OK)
                .and().body("status", equalTo("out-of-stock"));
    }

    @Test(dependsOnMethods = "verifyGetAction")
    public void verifyDeleteAction() {
        given().pathParam("petId", pet.getId()).delete(PET_ID).then().statusCode(HttpStatus.SC_OK);
        given().pathParam("petId",pet.getId()).get(PET_ID).then().statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @AfterClass
    public void tearDown() {
        given().get(USER_LOGOUT).then().statusCode(HttpStatus.SC_OK);
    }

    public class Pet {

        private String name;
        private List<String> photoUrls;

        private Long id;
        private Category category;
        private List<Tag> tags;
        private String status;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<String> getPhotoUrls() {
            return photoUrls;
        }

        public void setPhotoUrls(List<String> photoUrls) {
            this.photoUrls = photoUrls;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Category getCategory() {
            return category;
        }

        public void setCategory(Category category) {
            this.category = category;
        }

        public List<Tag> getTags() {
            return tags;
        }

        public void setTags(List<Tag> tags) {
            this.tags = tags;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    public class Category {

        private Long id;
        private String name;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class Tag {

        private Long id;
        private String name;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
