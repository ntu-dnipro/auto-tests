package org.nmu.atwd.lab.fourth;

import org.nmu.atwd.lab.fourth.generated.Calculator;
import org.nmu.atwd.lab.fourth.generated.CalculatorSoap;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SoapTest {

    private CalculatorSoap calculator;

    @BeforeClass
    public void setup() {
        Calculator factory = new Calculator();
        calculator = factory.getCalculatorSoap();
    }

    @DataProvider(name = "add")
    public static Object[][] addParams() {
        return new Object[][] {{12, 30, 42}, {1, 1, 2}, {20, 0, 20}};
    }

    @DataProvider(name = "sub")
    public static Object[][] subtractParams() {
        return new Object[][] {{30, 12, 18}, {1, 1, 0}, {20, 0, 20}};
    }

    @DataProvider(name = "mult")
    public static Object[][] multiplyParams() {
        return new Object[][] {{12, 30, 360}, {1, 1, 1}, {20, 0, 0}};
    }

    @DataProvider(name = "div")
    public static Object[][] divideParams() {
        return new Object[][] {{120, 30, 4}, {1, 1, 1}, {0, 20, 0}};
    }

    @Test(dataProvider = "add")
    public void testAdd(int x, int y, int result) {
        Assert.assertEquals(result, calculator.add(x, y));
    }

    @Test(dataProvider = "sub")
    public void testSubtract(int x, int y, int result) {
        Assert.assertEquals(result, calculator.subtract(x, y));
    }

    @Test(dataProvider = "mult")
    public void testMultiply(int x, int y, int result) {
        Assert.assertEquals(result, calculator.multiply(x, y));
    }

    @Test(dataProvider = "div")
    public void testDivide(int x, int y, int result) {
        Assert.assertEquals(result, calculator.divide(x, y));
    }
}
