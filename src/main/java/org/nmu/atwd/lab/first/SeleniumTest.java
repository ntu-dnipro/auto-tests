package org.nmu.atwd.lab.first;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.testng.Assert;
import org.testng.annotations.*;
import java.time.Duration;

public class SeleniumTest {

    private static final String BASE_URL = "https://www.nmu.org.ua/ua/";
    private static final Duration DEFAULT_WAIT_TIMEOUT = Duration.ofSeconds(15);

    private WebDriver safariDriver;

    @BeforeClass(alwaysRun = true)
    public void setUp() {
        WebDriverManager.safaridriver().setup();
        this.safariDriver = new SafariDriver(defaultOptions());
    }

    @BeforeMethod
    public void preconditions() {
        safariDriver.get(BASE_URL);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        if (safariDriver != null) {
            safariDriver.quit();
        }
    }

    @Test
    public void testHeaderExists() {
        WebElement header = safariDriver.findElement(By.id("heder"));
        Assert.assertNotNull(header, "Header doesn't exist");
    }

    @Test
    public void testClickOnForStudents() {
        WebElement forStudentsButton = safariDriver.findElement(By.xpath("html/body/center/div[4]/div/div[1]/ul/li[4]/a"));
        Assert.assertNotNull(forStudentsButton);
        forStudentsButton.click();
        Assert.assertNotEquals(safariDriver.getCurrentUrl(), BASE_URL);
    }

    @Test
    public void testSearchFieldOnForStudentsPage() {
        String studentsPageUrl = "content/student_life/students/";
        safariDriver.get(BASE_URL + studentsPageUrl);
        WebElement searchField = safariDriver.findElement(By.tagName("input"));
        Assert.assertNotNull(searchField);

        System.out.println(String.format("Name attribute %s", searchField.getAttribute("name")));
        System.out.println(String.format("ID attribute %s", searchField.getAttribute("id")));
        System.out.println(String.format("Type attribute %s", searchField.getAttribute("type")));
        System.out.println(String.format("Value attribute %s", searchField.getAttribute("value")));
        System.out.println(String.format("Position: (%d,%d)", searchField.getLocation().getX(), searchField.getLocation().getY()));
        System.out.println(String.format("Size: %dx%d", searchField.getSize().height, searchField.getSize().width));

        String inputValue = "I need info";
        searchField.sendKeys(inputValue);
        Assert.assertEquals(searchField.getText(), inputValue);

        searchField.sendKeys(Keys.ENTER);
        Assert.assertNotEquals(safariDriver.getCurrentUrl(), BASE_URL + studentsPageUrl);
    }

    @Test
    public void testSlider() {
        WebElement nextButton = safariDriver.findElement(By.className("next"));
        WebElement nextButtonByCss = safariDriver.findElement(By.cssSelector("a.next"));
        Assert.assertEquals(nextButton, nextButtonByCss);
        WebElement previousButton = safariDriver.findElement(By.className("prev"));
        while (!nextButton.getAttribute("class").contains("disabled")) {
            nextButton.click();
        }
        Assert.assertTrue(nextButton.getAttribute("class").contains("disabled"));
        Assert.assertFalse(previousButton.getAttribute("class").contains("disabled"));
        while (!previousButton.getAttribute("class").contains("disabled")) {
            previousButton.click();
        }
        Assert.assertTrue(previousButton.getAttribute("class").contains("disabled"));
        Assert.assertFalse(nextButton.getAttribute("class").contains("disabled"));
    }

    @Test
    public void testWiki() {
        //Відкриваємо сторінку Вікіпедії
        safariDriver.get("https://www.wikipedia.org");

        //Знаходимо елемент який відповідає полю пошуку. Перевіряємо, чи він знайдений і чи це саме поле пошуку
        WebElement searchField = safariDriver.findElement(By.xpath("//*[@id=\"searchInput\"]"));
        Assert.assertNotNull(searchField);
        Assert.assertEquals(searchField.getAttribute("type"), "search");

        //вводимо слово Dnipro в поле
        searchField.sendKeys("Dnipro");

//        Перевірка значення поля не працює в Safari браузері. Метод getText() повертає null.
//        При перевірці web inspector виявляється, що введені дані розміщуються во вкладеному <div> який скритий і тому не може бути знайдений
//        Assert.assertEquals(searchField.getText(), "Dnipro");

        //Знаходимо кнопку пошуку, перевіряємо, що вона знайдена і нажимаємо
        WebElement searchButton = safariDriver.findElement(By.xpath("//*[@id=\"search-form\"]/fieldset/button"));
        Assert.assertNotNull(searchButton);
        searchButton.click();

        //Переконуємося, що відкрилася сторінка з інформацією про місто Дніпро
        Assert.assertEquals(safariDriver.getCurrentUrl(), "https://en.wikipedia.org/wiki/Dnipro");
    }

    private SafariOptions defaultOptions() {
        SafariOptions options = new SafariOptions();
        options.setImplicitWaitTimeout(DEFAULT_WAIT_TIMEOUT);
        return options;
    }
}
